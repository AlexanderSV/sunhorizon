This is a slight alteration to the Default theme included in Screwturn. It was developed for an Inhouse Document Management system / Knowledge Base.

- Removed Background Image.
- Removed borders from content and other areas to 'clean up' the view.
- Wiki will be centered on the Browser window, irrespective of Screen resolution.

This has been tested on IE6 and IE7, but not Firefox. 

For information on how to implement the Coloured Bars within your wiki, see the following Forum Thread:

http://www.screwturn.eu/forum/viewtopic.php?t=4124

Posted as Public Domain, use and abuse it as you will, if you have any questions,  suggestions etc, use the Screwturn Forum to contact me or post questions.
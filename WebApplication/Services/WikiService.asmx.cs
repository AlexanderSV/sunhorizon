﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using ScrewTurn.Wiki.PluginFramework;

namespace ScrewTurn.Wiki.Services
{
    /// <summary>
    /// Summary description for WikiService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [ScriptService]
    public class WikiService : WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string[] GetNamespaces()
        {
            string currentWiki = Tools.DetectCurrentWiki();
            List<String> lstNamespace = new List<string>();
            lstNamespace.Add("root");
            lstNamespace.Add("<root>");
            foreach (NamespaceInfo ns in Pages.GetNamespaces(currentWiki))
            {
                lstNamespace.Add(Tools.UrlEncode(ns.Name));
                lstNamespace.Add(ns.Name);
            }
            return lstNamespace.ToArray();
        }

        [WebMethod(EnableSession = true)]
        public string[] GetPages(string selectedNs)
        {
            List<String> result = new List<String>(100);

            string currentWiki = Tools.DetectCurrentWiki();
            NamespaceInfo selectedNamespace = Pages.FindNamespace(currentWiki, Tools.UrlDecode(selectedNs));
            //NamespaceInfo currentNamespace = Tools.DetectCurrentNamespaceInfo();

            foreach (PageContent pi in Pages.GetPages(currentWiki, selectedNamespace))
            {
                string formattedTitle = FormattingPipeline.PrepareTitle(currentWiki, pi.Title, false, FormattingContext.Other, pi.FullName);
                //TreeElement item = new TreeElement((selectedNamespace != currentNamespace ? "++" : "") + pi.FullName, formattedTitle, onClickJavascript);
                
                //result.Add((selectedNamespace != currentNamespace ? "++" : "") + Tools.UrlEncode(pi.FullName)); // 
                result.Add(Tools.UrlEncode(pi.FullName));
                result.Add(formattedTitle);
            }
            return result.ToArray();
        }

        [WebMethod(EnableSession = true)]
        public string[] GetFilesStorageProviders()
        {
            List<String> result = new List<String>(2);

            string currentWiki = Tools.DetectCurrentWiki();
            IFilesStorageProviderV40[] provs = Collectors.CollectorsBox.FilesProviderCollector.GetAllProviders(currentWiki);
            foreach (IFilesStorageProviderV40 p in provs)
            {
                result.Add(p.GetType().FullName);
                result.Add(p.Information.Name);
                //lstProviderFiles.Items.Add(new ListItem(p.Information.Name, p.GetType().FullName));
                //// Select the default files provider
                //if (p.GetType().FullName == GlobalSettings.DefaultFilesProvider)
                //{
                //    lstProviderFiles.Items[lstProviderFiles.Items.Count - 1].Selected = true;
                //}
                //lstProviderImages.Items.Add(new ListItem(p.Information.Name, p.GetType().FullName));
                //// Select the default images provider
                //if (p.GetType().FullName == GlobalSettings.DefaultFilesProvider)
                //{
                //    lstProviderImages.Items[lstProviderImages.Items.Count - 1].Selected = true;
                //}
            }
            return result.ToArray();
        }

        [WebMethod(EnableSession = true)]
        public string[] GetImages(string provider, string page)
        {
            string currentWiki = Tools.DetectCurrentWiki();
            IFilesStorageProviderV40 p = Collectors.CollectorsBox.FilesProviderCollector.GetProvider(provider, currentWiki);
            return BuildImagesSubTree(p, Tools.UrlDecode(page)).ToArray(); // "/");
        }

        [WebMethod(EnableSession = true)]
        public string[] GetFiles(string page)
        {
            var files = new string[] {};
            string currentWiki = Tools.DetectCurrentWiki();
            foreach (IFilesStorageProviderV40 provider in Collectors.CollectorsBox.FilesProviderCollector.GetAllProviders(currentWiki))
            {
                var f = provider.ListPageAttachments(Tools.UrlDecode(page));
                if (f == null || f.Length == 0)
                    continue;
                files = files.Union(f).ToArray();
            }
            return files;
        }

        //[WebMethod(EnableSession = true)]
        //public string[] GetFiles(string provider, string page)
        //{
        //    string currentWiki = Tools.DetectCurrentWiki();
        //    IFilesStorageProviderV40 p = Collectors.CollectorsBox.FilesProviderCollector.GetProvider(provider, currentWiki);
        //    string[] files = p.ListPageAttachments(Tools.UrlDecode(page));
        //    return files.ToArray();
        //}

        private List<String> BuildImagesSubTree(IFilesStorageProviderV40 provider, string page) //, string path)
        {
            string[] files = new string[0];

            files = provider.ListPageAttachments(page);

            List<String> result = new List<String>(100);
            foreach (string f in files)
                if (ImageHelper.IsImage(f))
                    result.Add(f);
            return result;
        }


        #region "Snippets"

        [WebMethod(EnableSession = true)]
        public string[] GetSnippets()
        {
            string currentWiki = Tools.DetectCurrentWiki();
            var result = new List<String>();
            foreach (Snippet s in Snippets.GetSnippets(currentWiki))
            {
                string[] parameters = Snippets.ExtractParameterNames(s);
                int paramCount = parameters.Length;
                if (paramCount == 0)
                {
                    result.Add(string.Format("{{s:{0}}}", s.Name)); //("&#0123;s:{0}&#0125;", s.Name));
                    result.Add(s.Name);
                }
                else
                {
                    bool isPositional = IsSnippetPositional(parameters);
                    result.Add(string.Format("{{s:{0}{1}{2}}}", s.Name, isPositional ? "" : " ", GetParametersPlaceHolders(parameters, isPositional))); // "\\r\\n"
                    result.Add(string.Format("{0} ({1} {2})", s.Name, paramCount, Properties.Messages.Parameters));
                }
            }
            return result.ToArray();
        }

        /// <summary>
        /// Determines whether the parameters of a snippet are positional or not.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns><c>true</c> if the parameters are positional, <c>false</c> otherwise.</returns>
        private static bool IsSnippetPositional(string[] parameters)
        {
            int dummy;
            for (int i = 0; i < parameters.Length; i++)
            {
                if (!int.TryParse(parameters[i], out dummy)) return false;
                if (dummy != i + 1) return false;
            }
            return true;
        }

        /// <summary>
        /// Gets the placeholder for snippet parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="isSnippetPositional">A value indicating whether the snippet parameters are positional.</param>
        /// <returns>The snippet placeholder/template.</returns>
        private static string GetParametersPlaceHolders(string[] parameters, bool isSnippetPositional)
        {
            if (parameters.Length == 0) return "";
            else
            {
                var sb = new StringBuilder(20);
                foreach (string param in parameters)
                {
                    if (isSnippetPositional) sb.AppendFormat("|PLACE YOUR VALUE HERE ({0})", param);
                    else sb.AppendFormat("| {0} = PLACE YOUR VALUE HERE", param); // "\\r\\n"
                }
                return sb.ToString();
            }
        }

        #endregion "Snippets"

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using ScrewTurn.Wiki.PluginFramework;

namespace ScrewTurn.Wiki.Handlers
{
    public class EditorPluginsFileHandler : IHttpHandler
    {
        /// <summary>
        /// 
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            string user = SessionFacade.GetCurrentUsername();
            if (user == SessionFacade.AnonymousUsername)
            {
                context.Response.StatusCode = 401;
                context.Response.Write("Unauthorized");
                return;
            }

            string currentWiki = Tools.DetectCurrentWiki();
            IList<IFormatterProviderV40> providers = FormattingPipeline.GetSortedFormatters(currentWiki);

            if (context.Request.Path.EndsWith("wiki_config.js"))
            {
                var document = new StringBuilder();
                var index = context.Request.Url.AbsoluteUri.IndexOf("/ckeditor/", StringComparison.InvariantCulture);
                var customPluginsRoot = context.Request.Url.AbsoluteUri.Substring(0, index);// context.Request.Url.Authority;
                foreach (IFormatterProviderV40 provider in providers)
                {
                    if (provider.EnablePluginsEditor)
                    {
                        foreach (var pluginName in provider.PluginsEditroNames)
                        {
                            var providerName = FormatName(provider.Information.Name);
                            document.AppendFormat("CKEDITOR.plugins.addExternal('{0}', '{1}/ckeditor/custom_wiki_plugins/{2}/{0}/', 'plugin.js');", pluginName, customPluginsRoot, providerName);
                            document.AppendLine("CKEDITOR.editorConfig = function( config ) {");
                            document.AppendFormat("config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + '{0}';", pluginName);
                            document.AppendLine("};");                            
                        }
                    }
                }
                context.Response.Write(document);
            }
            else
            {
                if (!context.Request.Path.Contains(@"ckeditor/"))
                {
                    //context.Response.StatusCode = 404;
                    //context.Response.Write("Incorrect path");
                    // TODO: to log!
                    return;
                }
                var index = context.Request.Path.IndexOf(@"custom_wiki_plugins/", StringComparison.InvariantCulture);
                var path = context.Request.Path.Substring(index + 20);
                index = path.IndexOf(@"/", StringComparison.InvariantCulture);
                if (index <= 0)
                {
                    //context.Response.StatusCode = 404;
                    //context.Response.Write("Incorrect path");
                    // TODO: to log!
                    return;  
                }
                var pluginName = path.Substring(0, index);
                foreach (IFormatterProviderV40 provider in providers)
                {
                    if (provider.EnablePluginsEditor)
                    {
                        if (pluginName == FormatName(provider.Information.Name))
                        {
                            if (!provider.GetEditorsFile(path.Substring(index + 1), context.Response.OutputStream))
                            {
                                //context.Response.StatusCode = 404;
                                //context.Response.Write("Incorrect path");
                                // TODO: to log!
                            }
                            break;
                        }
                    }
                }

                //string fileName = "";
                //if (context.Request.Path.EndsWith("plugin.js"))
                //    fileName = @"ckeditor\wiki_plugins\wiki_link\plugin.js";
                //else if (context.Request.Path.EndsWith("wiki_link.js"))
                //    fileName = @"ckeditor\wiki_plugins\wiki_link\dialogs\wiki_link.js";
                //else if (context.Request.Path.EndsWith("anchor.gif"))
                //{
                //    fileName = @"ckeditor\wiki_plugins\wiki_link\images\anchor.gif";
                //    //context.Response.ContentType = "image/gif";
                //}
                //fileName = context.Request.PhysicalApplicationPath + fileName;
                //var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                ////StreamCopy(fs, context.Response.OutputStream);
                ////context.Response.Write(fs.ReadToEnd());
                ////context.Response.AddHeader("content-length", fs.Peek().ToString());
            }

            context.Response.End();
        }

        /// <summary>
        /// Generates an automatic page name.
        /// </summary>
        /// <param name="name">The page title.</param>
        /// <returns>The name.</returns>
        private string FormatName(string name)
        {
            // Replace all non-alphanumeric characters with dashes
            if (name.Length == 0) return "";

            StringBuilder buffer = new StringBuilder(name.Length);

            foreach (char ch in name.Normalize(NormalizationForm.FormD).Replace("\"", "").Replace("'", ""))
            {
                var unicat = char.GetUnicodeCategory(ch);
                if (unicat == System.Globalization.UnicodeCategory.LowercaseLetter ||
                    unicat == System.Globalization.UnicodeCategory.UppercaseLetter ||
                    unicat == System.Globalization.UnicodeCategory.DecimalDigitNumber)
                {
                    buffer.Append(ch);
                }
                else if (unicat != System.Globalization.UnicodeCategory.NonSpacingMark) buffer.Append("_");
            }

            while (buffer.ToString().IndexOf("__") >= 0)
            {
                buffer.Replace("__", "_");
            }

            return buffer.ToString().Trim('_');
        }

        #endregion
    }
}

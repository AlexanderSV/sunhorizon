﻿CKEDITOR.plugins.add('wiki_snippets',
	{
	    requires: ['richcombo'], //, 'styles' ],
	    lang: ['en', 'ru'],
	    init: function (editor) {
	        var config = editor.config,
			    lang = editor.lang.format;

	        var lng = editor.lang.wiki_snippets;

	        // Create style objects for all defined styles.
	        editor.ui.addRichCombo('Wiki_snippets',
				{
				    label: lng.label,
				    title: lng.label,
				    voiceLabel: lng.label,
				    className: 'cke_format',
				    multiSelect: false,

				    panel:
					{
					    css: [config.contentsCss, CKEDITOR.getUrl(editor.skinPath + 'editor.css')],
					    voiceLabel: lang.panelVoiceLabel
					},

				    init: function () {
				        this.startGroup(lng.label);
				        $.ajax({
				            context: this,
				            type: "POST",
				            url: ServicePath + 'GetSnippets',
				            data: "{}",
				            contentType: "application/json; charset=utf-8",
				            dataType: "json",
				            success: function (response) {
				                for (var ii = 0; ii < response.d.length; ii += 2) {
				                    this.add(response.d[ii], response.d[ii + 1], response.d[ii + 1]);
				                }
				            },
				            async: false,
				            error: function (data) {
				                var message = "Error";
				                if (typeof data !== "undefined")
				                    message += ": " + data.message;
				                alert(message);
				            },
				        });
				    },

				    onClick: function (value) {
				        editor.focus();
				        editor.fire('saveSnapshot');
				        editor.insertHtml(value);
				        editor.fire('saveSnapshot');
				    }
				});
	    }
	});
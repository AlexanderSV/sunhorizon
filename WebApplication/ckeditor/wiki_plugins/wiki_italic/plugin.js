﻿CKEDITOR.plugins.add('wiki_italic',
{
    init: function (editor) {
        editor.addCommand('wiki_italic', {
            exec: function(editor) {
                var selectedText = editor.getSelection().getNative();
                editor.insertText("''" + selectedText + "''");
            }
        });
        editor.ui.addButton('Wiki_italic',
            {
                label: editor.lang.italic,
                command: 'wiki_italic',
                icon: this.path + 'italic.png'
            });
    }
});


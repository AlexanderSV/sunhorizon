﻿CKEDITOR.plugins.add('wiki_h3',
{
	init: function (editor) {
	    editor.addCommand('wiki_h3', {
	        exec: function(editor) {
	            var selectedText = editor.getSelection().getNative();
	            editor.insertText("====" + selectedText + "====");
	        }
	    });
	    editor.ui.addButton('Wiki_h3',
            {
                label: editor.lang.format.tag_h3,
                command: 'wiki_h3',
            	icon: this.path + 'h3.png'
            });
           }
});


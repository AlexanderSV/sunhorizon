﻿CKEDITOR.plugins.add('wiki_striked',
{
    init: function (editor) {
        editor.addCommand('wiki_striked', {
            exec: function(editor) {
                var selectedText = editor.getSelection().getNative();
                editor.insertText("--" + selectedText + "--");
            }
        });
        editor.ui.addButton('Wiki_striked',
            {
                label: editor.lang.strike,
                command: 'wiki_striked',
                icon: this.path + 'striked.png'
            });
    }
});


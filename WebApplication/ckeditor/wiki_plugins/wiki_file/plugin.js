﻿CKEDITOR.plugins.add('wiki_file',
{
    requires: ['iframedialog'],
    lang: ['en', 'ru'],
    init: function (editor) {
        var pluginName = 'wiki_file';
        editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
        CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/wiki_file.js');
        editor.ui.addButton('Wiki_file',
            {
                label: editor.lang.wiki_file.buttonName,
                command: pluginName,
                icon: this.path + 'images/file.png'
            });

        if (editor.addMenuItems) {
            editor.addMenuItems(
				{
				    wikifile:
					{
					    label: editor.lang.wiki_file.menuItemName,
					    command: pluginName,
					    icon: this.path + 'images/file.png',
					    group: 'wikilink' // add to config
					}
				});
        }
        if (editor.contextMenu) {
            editor.contextMenu.addListener(function (q) {
                if (q && !q.isReadOnly()) {
                    var r = q.getName();
                    //debugger;
                    if (r == 'a' && q.getAttribute('wikiid') && q.getAttribute('wikiid') == 'file')
                        return { wikifile: CKEDITOR.TRISTATE_OFF };
                    return null;
                }
                //				if (!element || !element.is('img'))
                //					return null;
                //				return { wikiimage: CKEDITOR.TRISTATE_OFF };
            });
        }

    }
});
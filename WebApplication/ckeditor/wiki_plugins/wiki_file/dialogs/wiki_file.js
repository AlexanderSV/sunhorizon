﻿CKEDITOR.dialog.add('wiki_file', function (editor) {
    var curNs = CurrentNamespace && CurrentNamespace != "" ? CurrentNamespace : "root";
    var page = CurrentPage;
    var qparams;
    
    var loadFiles = function() {
        var wp = this.getDialog().getContentElement('info', 'wikiPages');
        if (wp && wp.getValue().length > 0) {
            var pg = wp.getValue();
            //if (pg.indexOf('++') == 0) pg = pg.substring(2, pg.length);
            $.ajax({
                context: this,
                type: "POST",
                url: ServicePath + 'GetFiles',
                data: "{ \"page\": \"" + pg + "\" }",//postData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    var I = this.getDialog().getContentElement('info', 'wikiFiles');
                    I.clear();
                    for (var ii = 0; ii < response.d.length; ii += 1) {
                        I.add(response.d[ii]);
                    }
                    var im = qparams['File'];
                    if (im && im.length > 0) {
                        var index = im.indexOf('|');
                        if (index >= 0) im = im.substring(0, index);
                        I.setValue(im);
                    }
                },
                async: false,
                error: function(data) {
                    var message = E.error;
                    if (typeof data !== "undefined")
                        message += ": " + data.message;
                    alert(message);
                }
            });
        }
    },
        parseQueryString = function (queryString) {
            var params = {}, queries, temp, ii, ln;
            queries = queryString.split("&");
            for (ii = 0, ln = queries.length; ii < ln; ii++) {
                temp = queries[ii].split('=');
                params[temp[0]] = temp[1];
            }
            return params;
        };

    var lng = editor.lang.wiki_file;
    return {
        title: lng.title,
        minWidth: 300,
        minHeight: 350,
        onShow: function () {
            var params = {};
            var t = this;
            t.editMode = false;
            var parent = t.getParentEditor(),
			    selection = parent.getSelection(),
                element = selection && selection.getStartElement();
            if (element && element.getName() == 'a' && element.getAttribute('wikiid') && element.getAttribute('wikiid') == 'file') {
                t.editMode = true;
                t.element = element;

                params.title = element.getAttribute('title');
                var query = element.getAttribute('href');
                var queryString = query.substring(query.indexOf('?') + 1);
                qparams = parseQueryString(queryString);

                page = qparams["Page"];
                /* remove PageExtension */
                if (PageExtension && PageExtension.length > 0) {
                    pos = page.indexOf(PageExtension);
                    if (-1 < pos) page = this.substring(0, pos);
                }
                var reg = page.match(/^(?:\+\+)?(.*)$/);
                /* Get namespace */
                var pos = reg[1].indexOf('.');
                if (-1 < pos) {
                    curNs = reg[1].substring(0, pos);
                } else {
                    curNs = CurrentNamespace && CurrentNamespace != "" ? CurrentNamespace : "root";
                }
  
            }
            this.setupContent(params);
        },
        onOk: function () {
            var t = this;
            var result = {};
            t.commitContent(result);

            if (IsInWYSIWYG) {              
                var params = {};               
                params.href = "GetFile.aspx?Page=" + result.page + "&File=" + result.file;
                params['title'] = result.title;                
                
                if (t.editMode) {
                    t.element.setText(result.title);
                    params['data-cke-saved-href'] = params.href;
                    t.element.setAttributes(params);
                } else {
                    t.element = editor.document.createElement('a');
                    params['class'] = 'internallink';
                    params['wikiid'] = 'file';
                    t.element.setAttributes(params);
                    t.element.setText(result.title);
                    editor.insertElement(t.element);
                }
            } else {
                //editor.insertText("<div page=\"" + pageName + "\" pluginid=\"" + pluginid + "\" style=\"" + style + "\"></div>");
                //var title = '';
                //if (result.title && result.title.length > 0 && result.title != result.file) {
                //    title = '|' + result.title;
                //}
                editor.insertText("[file:{UP(" + decodeURI(result.page) + ")}" + decodeURI(result.file) + '|' + result.title + "]");
            }
        },
        contents: [{
            id: 'info',
            //label: lng.info,
            //title: lng.info,
            elements: [{
                type: 'vbox',
                id: 'ratingOptions',
                children: [{
                    id: 'wikiNsList',
                    label: lng.namespaces,
                    type: 'select',
                    style: 'width: 100%;',
                    items: [['']],
                    setup: function () {
                        $.ajax({
                            context: this,
                            type: "POST",
                            url: ServicePath + 'GetNamespaces',
                            data: "{}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var t = this;
                                t.clear();
                                for (var ii = 0; ii < response.d.length; ii += 2) {
                                    t.add(response.d[ii + 1], response.d[ii]);
                                }
                                t.setValue(curNs);
                            },
                            async: false,
                            error: function (data) {
                                var message = "Error";
                                if (typeof data !== "undefined")
                                    message += ": " + data.message;
                                alert(message);
                            },
                        });
                    },
                    onChange: function () {
                        $.ajax({
                            context: this,
                            type: "POST",
                            url: ServicePath + 'GetPages',
                            data: "{ selectedNs: '" + this.getValue() + "' }",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var I = this.getDialog().getContentElement('info', 'wikiPages');
                                I.clear();
                                for (var ii = 0; ii < response.d.length; ii += 2) {
                                    I.add(response.d[ii + 1], response.d[ii]);
                                }
                                //pages = response.d;
                                if (page && page.length > 0) {
                                    I.setValue(page);
                                } else {
                                    I.setValue(CurrentPage);
                                }
                            },
                            async: false,
                            error: function (data) {
                                var message = "Error";
                                if (typeof data !== "undefined")
                                    message += ": " + data.message;
                                alert(message);
                            },
                        });
                    },
                    commit: function (result) {
                        result.namespace = this.getValue();
                    }
                }, {
                    id: 'wikiPages',
                    label: lng.pages,
                    type: 'select',
                    style: 'width: 100%;',
                    items: [['']],
                    size: 7,
                    onChange: function () {
                        loadFiles.call(this);
                    },
                    commit: function (result) {
                        result.page = this.getValue();
                    }
                }, {
                    id: 'wikiFiles',
                    label: lng.files,
                    type: 'select',
                    style: 'width: 100%;',
                    items: [['']],
                    size: 7,
                    onChange: function () {
                        var el = this.getDialog().getContentElement('info', 'title');
                        if (!el.getValue())
                            el.setValue(this.getValue());
                    },
                    commit: function (result) {
                        result.file = this.getValue();
                    },
                }, {
                    id: 'title',
                    type: 'text',
                    label: lng.name,
                    required: true,
                    style: 'width: 100%',
                    setup: function (params) {
                        if (params) this.setValue(params.title);
                    },
                    commit: function (result) {
                        result.title = this.getValue();
                    }
                }]
            }]
        }]

    };
});

﻿CKEDITOR.plugins.add('wiki_h1',
{
	init: function (editor) {
	    editor.addCommand('wiki_h1', {
	        exec: function(editor) {
	            var selectedText = editor.getSelection().getNative();
	            editor.insertText("==" + selectedText + "==");
	        }
	    });
	    editor.ui.addButton('Wiki_h1',
            {
                label: editor.lang.format.tag_h1,
                command: 'wiki_h1',
            	icon: this.path + 'h1.png'
            });
           }
});


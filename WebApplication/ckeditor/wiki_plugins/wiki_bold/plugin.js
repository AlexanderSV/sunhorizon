﻿CKEDITOR.plugins.add('wiki_bold',
{
	init: function (editor) {
	    editor.addCommand('wiki_bold', {
	        exec: function(editor) {
	            var selectedText = editor.getSelection().getNative();
	            editor.insertText("'''" + selectedText + "'''");
	        }
	    });
	    editor.ui.addButton('Wiki_bold',
            {
                label: editor.lang.bold,
                command: 'wiki_bold',
            	icon: this.path + 'bold.png'
            });
           }
});


﻿CKEDITOR.plugins.setLang('wiki_link', 'ru', {
    wiki_link: {
        buttonName: 'Вставка/редактирование ссылки',
        menuItemName: 'Редактирование ссылки',
        targetFrameName: 'Имя целевого фрейма',
        targetPopupName: 'Имя всплывающего окна',
        protocol: 'Протокол',
        url: 'Ссылка',
        browseServer: 'Выбор на сервере',
        target: 'Цель',
        notSet: '<не указано>',
        targetNew: 'Новое окно (_blank)',
        title: 'Ссылка',
        info: 'Информация о ссылке',
        type: 'Тип ссылки',
        toWikiUrl: 'Ссылка на wiki-страницу',
        toUrl: 'Ссылка',
        toAnchor: 'Ссылка на якорь в тексте',
        toEmail: 'Email',
        name: 'Имя',
        wikiTitles: 'Статьи',
        noUrl: 'Пожалуйста, введите ссылку',
        other: '<другой>',
        selectAnchor: 'Выберите якорь',
        anchorName: 'По имени',
        anchorId: 'По идентификатору',
        noAnchors: '(В документе нет ни одного якоря)',
        emailAddress: 'Email адрес',
        emailSubject: 'Тема сообщения',
        emailBody: 'Текст сообщения',
        noEmail: 'Пожалуйста, введите email адрес',
        namespace: 'Раздел',
    }
});
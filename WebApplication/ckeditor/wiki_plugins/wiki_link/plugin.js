﻿CKEDITOR.plugins.add('wiki_link',
{
    requires: ['iframedialog'],
    lang: ['en', 'ru'],
    init: function (editor) {

        //		var o = function(q, r, s) {
        //			editor.addCommand(r, new CKEDITOR.dialogCommand(r));
        //			editor.ui.addButton(q, {
        //				label: 'Insert image', //n.common[q.charAt(0).toLowerCase() + q.slice(1)],
        //				command: r,
        //				icon: this.path + 'images/anchor.gif'
        //			});
        //			CKEDITOR.dialog.add(r, s);
        //		};

        //		o('Wiki_image', 'wiki_image', this.path + 'dialogs/wiki_image.js');
        //		o('Wiki_imagebutton', 'wiki_imagebutton', this.path + 'dialogs/wiki_image.js');

        var pluginName = 'wiki_link';
        editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
        CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/wiki_link.js');
        editor.ui.addButton('Wiki_link',
            {
                label: editor.lang.wiki_link.buttonName,
                command: pluginName,
                icon: this.path + 'images/link.gif'
            });

        if (editor.addMenuItems) {
            editor.addMenuItems(
				{
				    wikilink:
					{
					    label: editor.lang.wiki_link.menuItemName,
					    command: pluginName,
					    icon: this.path + 'images/link.gif',
					    group: 'wikilink' // add to config
					}
				});
        }
        if (editor.contextMenu) {
            editor.contextMenu.addListener(function (q) {
                if (q && !q.isReadOnly()) {
                    var r = q.getName();
                    if (r == 'a' && !q.getAttribute('wikiid')) //r == 'input' && q.getAttribute('type') == 'image')
                        return { wikilink: CKEDITOR.TRISTATE_OFF };
                }
                //				if (!element || !element.is('img'))
                //					return null;
                //				return { wikiimage: CKEDITOR.TRISTATE_OFF };
            });
        }

    }
});




//CKEDITOR.plugins.add('wiki_link',
//{
//    requires: ['iframedialog'],
//	init: function (editor) {
//	    var pluginName = 'wiki_link';
//	    editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
//	    CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/wiki_link.js');	    
//	    editor.ui.addButton('Wiki_link',
//            {
//            	label: 'Insert/edit link',
//            	command: pluginName,
//            	icon: this.path + 'images/anchor.gif'
//            });
	    

//	}
//});


//CKEDITOR.plugins.add('wiki_link',
//    {
//    	requires: ['iframedialog'],
//    	init: function (editor) {
//    		var pluginName = 'wiki_link';
//    		var mypath = this.path;
//    		editor.ui.addButton(
//                'wiki_link.btn',
//                {
//                	label: "My Plug-in",
//                	command: 'wiki_link.cmd',
//                	icon: mypath + 'images/anchor.gif'
//                }
//            );
//                var cmd = editor.addCommand('wiki_link.cmd', { exec: showDialogPlugin });
//    		CKEDITOR.dialog.addIframe(
//                'wiki_link.dlg',
//                'Hello Title',
//                mypath + 'wiki_link.html',
//                400,
//                300,
//                function () {
//                }
//            );
//    	}
//    }
//);

//      function showDialogPlugin(e) {
//      	e.openDialog('wiki_link.dlg');
//      }
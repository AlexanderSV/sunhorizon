﻿CKEDITOR.plugins.add('wiki_codeinline',
{
	init: function (editor) {
	    editor.addCommand('wiki_codeinline', {
	        exec: function(editor) {
	            var selectedText = editor.getSelection().getNative();
	            editor.insertText("{{" + selectedText + "}}");
	        }
	    });
	    editor.ui.addButton('Wiki_codeinline',
            {
                label: editor.lang.format.tag_h1,
                command: 'wiki_codeinline',
                icon: this.path + 'codeinline.png'
            });
           }
});


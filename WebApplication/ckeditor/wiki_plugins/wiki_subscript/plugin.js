﻿CKEDITOR.plugins.add('wiki_subscript',
{
    init: function (editor) {
        editor.addCommand('wiki_subscript', {
            exec: function(editor) {
                var selectedText = editor.getSelection().getNative();
                editor.insertText("<sub>" + selectedText + "</sub>");
            }
        });
        editor.ui.addButton('Wiki_subscript',
            {
                label: editor.lang.subscript,
                command: 'wiki_subscript',
                icon: this.path + 'subscript.png'
            });
    }
});


﻿CKEDITOR.plugins.add('wiki_superscript',
{
    init: function (editor) {
        editor.addCommand('wiki_superscript', {
            exec: function(editor) {
                var selectedText = editor.getSelection().getNative();
                editor.insertText("<sup>" + selectedText + "</sup>");
            }
        });
        editor.ui.addButton('Wiki_superscript',
            {
                label: editor.lang.superscript,
                command: 'wiki_superscript',
                icon: this.path + 'superscript.png'
            });
    }
});


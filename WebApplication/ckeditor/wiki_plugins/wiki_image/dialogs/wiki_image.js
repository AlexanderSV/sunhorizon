﻿
CKEDITOR.dialog.add('wiki_image', function (a) {
    var curNs = CurrentNamespace && CurrentNamespace != "" ? CurrentNamespace : "root";
    var page = CurrentPage;
    //var pages;
    var qparams;

	var n;
	function o(B) {
		if (n) return;
		n = 1;
		var C = this.getDialog(),
		    D = C.imageElement;
		if (D) {
		    this.commit(IMAGE, D);
			B = [].concat(B);
			var H = B.length,
			    F;
			for (var G = 0; G < H; G++) {
				F = C.getContentElement.apply(C, B[G].split(':'));
				F && F.setup(IMAGE, D);
			}
		}
		n = 0;
	};

	function m() {
		var B = arguments;
		this.foreach(function(D) {
			if (D.commit) D.commit.apply(D, B);
		});
	};

	var v = function(B) {
		return CKEDITOR.tools.getNextId() + '_' + B;
	}, 
	    IMAGE = 1,
        //e = 2,
        PREVIEW = 4,
        g = 8,
	    h = /^\s*(\d+)((px)|\%)?\s*$/i,
	    i = /(^\s*(\d+)((px)|\%)?\s*$)|^$/i,
	    k = function () {
		    var B = this.getValue(),
		        C = this.getDialog(),
		        D = B.match(h);
		    if (D) {
			    if (D[2] == '%') p(C, false);
			    B = D[1];
		    }
		    if (C.lockRatio) {
			    var F = C.originalElement;
			    if (F.getCustomData('isReady') == 'true')
				    if (this.id == 'txtHeight') {
					    if (B && B != '0') B = Math.round(F.$.width * (B / F.$.height));
					    if (!isNaN(B)) C.setValueOf('info', 'txtWidth', B);
				    } else {
					    if (B && B != '0') B = Math.round(F.$.height * (B / F.$.width));
					    if (!isNaN(B)) C.setValueOf('info', 'txtHeight', B);
				    }
		    }
		    l(C);
	    },
	    l = function (B) {
	        if (!B.originalElement || !B.preview) return 1;
	        B.commitContent(PREVIEW, B.preview);
	        return 0;
	    },
	ccw = function () {
	    var G = this.getValue();
	    var L = this.getDialog().getContentElement('info', 'customSize');
	    if (L) {
	        L = L.getElement().getParent().getParent();
	        H = this.getDialog().getContentElement('info', 'txtCustomSize');
	        if (G == 'customWidth' || G == 'customMax') {
	            L.show();
	            if (G == 'customWidth') H.setLabel(E.width);
	            else H.setLabel(E.sizeMax);
	        } else L.hide();
	    }
	},
	lp = function() {
	    var fp = this.getDialog().getContentElement('info', 'fileProv');
	    var wp = this.getDialog().getContentElement('info', 'wikiPages');
	    if (fp && fp.getValue().length > 0 && wp && wp.getValue().length > 0) {
	        var pg = wp.getValue();
	        //if (pg.indexOf('++') == 0) pg = pg.substring(2, pg.length);
	        $.ajax({
	            context: this,
	            type: "POST",
	            url: ServicePath + 'GetImages',
	            data: "{ \"provider\": \"" + fp.getValue() + "\",\"page\": \"" + pg + "\" }",//postData,
	            contentType: "application/json; charset=utf-8",
	            dataType: "json",
	            success: function(response) {
	                var I = this.getDialog().getContentElement('info', 'wikiImages');
	                I.clear();
	                for (var ii = 0; ii < response.d.length; ii += 1) {
	                    I.add(response.d[ii]);
	                }
	                var im = qparams['File'];
	                if (im && im.length > 0) {
	                    var index = im.indexOf('|');
	                    if (index >= 0) im = im.substring(0, index);
	                    I.setValue(im);
	                }
	            },
	            async: false,
	            error: function(data) {
	                var message = E.error;
	                if (typeof data !== "undefined")
	                    message += ": " + data.message;
	                alert(message);
	            }
	        });
	    }
	},
    qs = function( queryString ) {
        var params = {}, queries, temp, ii, ln;
        queries = queryString.split("&");
        for ( ii = 0, ln = queries.length; ii < ln; ii++ ) {
            temp = queries[ii].split('=');
            params[temp[0]] = temp[1];
        }
        return params;
    },
	si = function() {
		var B = this.getDialog(),
		    url = '',
		    type = this.getDialog().getContentElement('info', 'linkType').getValue();
		if (type == 'url') url = this.getDialog().getContentElement('info', 'url').getValue(); 
		else url = cl.call(this);
		if (url.length > 0) {
			B.preview.removeStyle('display');
			if (type == 'url') {
				var D = B.originalElement;
				D.setCustomData('isReady', 'false');
				var F = CKEDITOR.document.getById(y);
				if (F) F.setStyle('display', '');
				D.on('load', t, B);
				D.on('error', u, B);
				D.on('abort', u, B);
				D.setAttribute('src', url);				
			}

			//s.setAttribute('src', C);
			//B.preview.setAttribute('src', s.$.src);
			l(B);
			B.preview.setAttribute('src', url);
		} else if (B.preview) {
			B.preview.removeAttribute('src');
			B.preview.setStyle('display', 'none');
		}
	},
	cl = function() {
	    // build wiki-url
	    var wp = this.getDialog().getContentElement('info', 'wikiPages');
	    var wi = this.getDialog().getContentElement('info', 'wikiImages');
	    if (wi && wi.getValue().length > 0 && wp && wp.getValue().length > 0) {
	        var pg = wp.getValue();
	        //if (pg.indexOf('++') == 0) pg = pg.substring(2, pg.length);
	        var size = '|m300';
	        var s = this.getDialog().getContentElement('info', 'size');
	        var cs = this.getDialog().getContentElement('info', 'txtCustomSize');
	        switch (s.getValue()) {
	        case 'small':
	            size = '|w100';
	            break;
	        case 'medium':
	            size = '|w250';
	            break;
	        case 'full':
	            size = '';
	            break;
	        case 'customWidth':
	            if (cs.getValue().length > 0) size = '|w' + cs.getValue();
	            break;
	        case 'customMax':
	            if (cs.getValue().length > 0) size = '|m' + cs.getValue();
	            break;
	        default:
	        }
	        return "GetFile.aspx?Page=" + pg + "&File=" + wi.getValue() + size;
	    } else {
	        return "";
	    }
	}, t = function() {
		var B = this.originalElement;
		B.setCustomData('isReady', 'true');
		B.removeListener('load', t);
		B.removeListener('error', u);
		B.removeListener('abort', u);
		CKEDITOR.document.getById(y).setStyle('display', 'none');
		if (!this.dontResetSize) q(this);
		if (this.firstLoad)
			CKEDITOR.tools.setTimeout(function() {
				p(this, 'check');
			}, 0, this);
		this.firstLoad = false;
		this.dontResetSize = false;
	}, u = function() {
		var D = this;
		var B = D.originalElement;
		B.removeListener('load', t);
		B.removeListener('error', u);
		B.removeListener('abort', u);
		var C = CKEDITOR.getUrl(a.skinPath + 'images/noimage.png');
		if (D.preview) D.preview.setAttribute('src', C);
		CKEDITOR.document.getById(y).setStyle('display', 'none');
		p(D, false);
	},
	p = function (B, C) {
		if (!B.getContentElement('info', 'ratioLock')) return null;
		var D = B.originalElement;
		if (!D) return null;
		if (C == 'check') {
			if (!B.userlockRatio && D.getCustomData('isReady') == 'true') {
				var K = B.getValueOf('info', 'txtWidth'),
				    F = B.getValueOf('info', 'txtHeight'),
				    G = D.$.width * 1000 / D.$.height,
				    H = K * 1000 / F;
				B.lockRatio = false;
				if (!K && !F) B.lockRatio = true;
				else if (!isNaN(G) && !isNaN(H)) if (Math.round(G) == Math.round(H)) B.lockRatio = true;
			}
		} else if (C != undefined) B.lockRatio = C;
		else {
			B.userlockRatio = 1;
			B.lockRatio = !B.lockRatio;
		}
		var I = CKEDITOR.document.getById(w);
		if (B.lockRatio) I.removeClass('cke_btn_unlocked');
		else I.addClass('cke_btn_unlocked');
		I.setAttribute('aria-checked', B.lockRatio);
		if (CKEDITOR.env.hc) {
			var J = I.getChild(0);
			J.setHtml(B.lockRatio ? CKEDITOR.env.ie ? '■' : '▣' : CKEDITOR.env.ie ? '□' : '▢');
		}
		return B.lockRatio;
	},
	q = function(B) {
		var C = B.originalElement;
		if (C.getCustomData('isReady') == 'true') {
			var D = B.getContentElement('info', 'txtWidth'),
			    H = B.getContentElement('info', 'txtHeight');
			D && D.setValue(C.$.width);
			H && H.setValue(C.$.height);
		}
		l(B);
	},
	r = function (B, C) {
	    if (B != IMAGE) return;

	    function D(I, J) {
	        var K = I.match(h);
	        if (K) {
	            if (K[2] == '%') {
	                K[1] += '%';
	                p(E, false);
	            }
	            return K[1];
	        }
	        return J;
	    };
	    var L = this.getDialog(),
            F = '',
            G = this.id == 'txtWidth' ? 'width' : 'height',
            H = C.getAttribute(G);
	    if (H) F = D(H, F);
	    F = D(C.getStyle(G), F);
	    this.setValue(F);
	},
    ct = function () {
			var F = this.getDialog(),
	        G = ['wikiOptions', 'urlOptions', 'wikiSize', 'urlSize'],
		    H = this.getValue();
	    	for (var K = 0; K < G.length; K++) {
	    		var L = F.getContentElement('info', G[K]);
	    		if (!L) continue;
	    		L = L.getElement().getParent().getParent();
	    		if (G[K].indexOf(H) == 0) L.show();
	    		else L.hide();
	    	}
	    	F.layout();
			si.call(this);
	    }, w = v('btnLockSizes'),
	    x = v('btnResetSize'),
	    y = v('ImagePreviewLoader'),
	    z = v('previewLink'),
	    A = v('previewImage');
	var E = a.lang.wiki_image;
	return {
		title: E.title,
		minWidth: 420,
		minHeight: 460,
		//buttons: [CKEDITOR.dialog.okButton, CKEDITOR.dialog.cancelButton],
//		onOk: function() {
//		    var C = this;
////		    if (C.imageEditMode) {
////		            C.imageElement = C.cleanImageElement;
////		            delete C.cleanImageElement;
////		    } else {
////		        C.imageElement = a.document.createElement('img');
////		        C.imageElement.setAttribute('alt', '');
////		    }
//		    C.commitContent(d, C.imageElement);
//		    if (!C.imageElement.getAttribute('style')) C.imageElement.removeAttribute('style');
//		    a.insertElement(C.imageElement);
//		},
		onOk: function () {
		    if (IsInWYSIWYG) {
		        if (this.imageEditMode) {
		            this.imageElement = this.cleanImageElement;
		            delete this.cleanImageElement;
		        } else {
		            this.imageElement = a.document.createElement('img');
		            this.imageElement.setAttribute('alt', '');
		            this.imageElement.setAttribute('wikiid', 'image');		            
		        }
		        this.commitContent(IMAGE, this.imageElement);
		        //this.commitContent(e, this.linkElement);
		        if (!this.imageElement.getAttribute('style')) this.imageElement.removeAttribute('style');
		        if (!this.imageEditMode) a.insertElement(this.imageElement);
		    } else {
		        //debugger;
		        this.imageElement.result = {};
		        this.commitContent(IMAGE, this.imageElement);
		        var result = this.imageElement.result;

		        var ilink = '';
		        if (this.imageElement.data('wiki-link')) ilink = '|link=' + this.imageElement.data('wiki-link');
		        var size = '';
		        if (this.imageElement.data('wiki-type-size') && (this.imageElement.data('wiki-type-size') == 'small' || this.imageElement.data('wiki-type-size') == 'medium'))
		            size = '|' + this.imageElement.data('wiki-type-size');
		        var align = '';
		        if (this.imageElement.data('wiki-align') && this.imageElement.data('wiki-align') != 'auto') align = '|' + this.imageElement.data('wiki-align');
		        var frame = '';
		        if (this.imageElement.data('wiki-border') && this.imageElement.data('wiki-border') != 'frame') frame = '|' + this.imageElement.data('wiki-border');
		        var csize = '';
		        if (this.imageElement.data('wiki-size-width') && this.imageElement.data('wiki-size-width') > 0 
		            && this.imageElement.data('wiki-type-size')
		            && (this.imageElement.data('wiki-url-type') || this.imageElement.data('wiki-url-type') != 'url')) {
		            if (this.imageElement.data('wiki-type-size') == 'customMax') csize = '|max' + this.imageElement.data('wiki-size-width');
		            if (this.imageElement.data('wiki-type-size') == 'customWidth') csize = '|' + this.imageElement.data('wiki-size-width');
		        }
		        if (this.imageElement.data('wiki-url-type') || this.imageElement.data('wiki-url-type') == 'url') {
		            if (result.width > 0 && result.height > 0) csize = '|' + result.width + 'x' + result.height;
		        }
		        var alt = '';
		        if (result.alt && result.alt.length > 0) alt = '|alt=' + result.alt;
		        var descr = '';
		        if (this.imageElement.data('wiki-desc') && this.imageElement.data('wiki-desc').length > 0) descr = '|' + this.imageElement.data('wiki-desc');
		        
		        a.insertText("[image:{UP(" + decodeURI(result.page) + ")}" + decodeURI(result.image) + ilink + size + align + frame + csize + alt + descr + "]");
		    }
		    
		},
		onLoad: function () {
			var C = this;
//			if (c != 'image') C.hidePage('Link');
			var B = C._.element.getDocument();
			if (C.getContentElement('info', 'ratioLock')) {
				C.addFocusable(B.getById(x), 5);
				C.addFocusable(B.getById(w), 5);
			}
			C.commitContent = m;
		},
		onShow: function () {
		    var H = this;
			H.imageEditMode = false;
			H.imageElement = false;
			H.lockRatio = true;
			H.userlockRatio = 0;
			H.dontResetSize = false;
			H.firstLoad = true;
			var B = H.getParentEditor(),
			    C = B.getSelection(),
			    D = C && C.getSelectedElement();
			CKEDITOR.document.getById(y).setStyle('display', 'none');
			//s = new CKEDITOR.dom.element('img', B.document);

			H.preview = CKEDITOR.document.getById(A);
			H.originalElement = B.document.createElement('img');
			H.originalElement.setAttribute('alt', '');
			H.originalElement.setCustomData('isReady', 'false');
			//if (D && D.getName() == 'img') {
			//    if (D.data('wiki-size')) this.getDialog().getContentElement('info', 'size').setValue(D.data('wiki-size'));
			//}
			if (D && D.getName() == 'img' && !D.data('cke-realelement')) {
				H.imageEditMode = D.getName();
				H.imageElement = D;
			}
		    
		        qparams = {};
		    if (H.imageEditMode) {
		        //H.imageEditMode = 'img';
		        //H.imageElement = D;
		        H.cleanImageElement = H.imageElement;
		        H.imageElement = H.cleanImageElement.clone(true, true);
		        //H.setupContent(d, H.imageElement);
		        var ie = H.imageElement;
		        if (ie.data('wiki-url-type') && ie.data('wiki-url-type') == 'wiki' && ie.getAttribute('src')) {
		            var query = ie.getAttribute('src');
		            var queryString = query.substring(query.indexOf('?') + 1);
		            qparams = qs(queryString);

		            page = qparams["Page"];
		            /* remove PageExtension */
		            if (PageExtension && PageExtension.length > 0) {
		                pos = page.indexOf(PageExtension);
		                if (-1 < pos) page = H.substring(0, pos);
		            }
		            var reg = page.match(/^(?:\+\+)?(.*)$/);
		            /* Get namespace */
		            var pos = reg[1].indexOf('.');
		            if (-1 < pos) curNs = reg[1].substring(0, pos)
		            else curNs = CurrentNamespace && CurrentNamespace != "" ? CurrentNamespace : "root";
		        }
		    } else {
		        H.imageElement = B.document.createElement('img');
		        page = "";
		    }

		    H.setupContent(IMAGE, H.imageElement);
		    p(H, true);
			
		},
		onHide: function () {
			if (this.preview) this.commitContent(g, this.preview);
			if (this.originalElement) {
			    this.originalElement.removeListener('load', t);
			    this.originalElement.removeListener('error', u);
			    this.originalElement.removeListener('abort', u);
			    this.originalElement.remove();
			    this.originalElement = false;
			}
			delete this.imageElement;
		},
		//onCancel: function () { },
		//resizable: none /* none,width,height or both  */,
		validate: function () {
			CKEDITOR.config.chili_val = this.getValue();
		},
		contents: [{
			id: 'info',
			label: E.info,
			title: E.info,
			elements: [{
				id: 'linkType',
				type: 'select',
				label: E.type,
				'default': 'wiki',
				items: [
						[E.toWikiImg, 'wiki'],
						[E.toUrl, 'url']
					],
				onChange: ct,
				setup: function (type, element) {
				    if (type == IMAGE && element.data('wiki-url-type')) this.setValue(element.data('wiki-url-type'));
				    else this.setValue('wiki');
				},
				commit: function (type, element) {
					if (type == IMAGE && (this.getValue() || this.isChanged())) element.data('wiki-url-type', this.getValue());
				}
			}, {
				type: 'vbox',
				id: 'wikiOptions',
				children: [{
					id: 'basic',
					type: 'hbox',
					height: '100%',
					widths: ['50%', '50%'],
					children: [{
						id: 'selPage',
						type: 'vbox',
						padding: 0,
						children: [{
						    id: 'wikiNsList',
							label: E.namespaces,
							type: 'select',
							style: 'width: 100%;',
							items: [['']],
							setup: function (type, element) {
							    $.ajax({
							        context: this,
							        type: "POST",
							        url: ServicePath + 'GetNamespaces',
							        data: "{}",
							        contentType: "application/json; charset=utf-8",
							        dataType: "json",
							        success: function (response) {
							            var I = this;
							            I.clear();
							            for (var ii = 0; ii < response.d.length; ii += 2) {
							                I.add(response.d[ii + 1], response.d[ii]);
							            }
							            I.setValue(curNs);
							        },
							        async: false,
							        error: function (data) {
							            var message = "Error";
							            if (typeof data !== "undefined")
							                message += ": " + data.message;
							            alert(message);
							        },
							    });
							},
							onChange: function () {
							    $.ajax({
							        context: this,
							        type: "POST",
							        url: ServicePath + 'GetPages',
							        data: "{ selectedNs: '" + this.getValue() + "' }",
							        contentType: "application/json; charset=utf-8",
							        dataType: "json",
							        success: function (response) {
							            var I = this.getDialog().getContentElement('info', 'wikiPages');
							            I.clear();
							            for (var ii = 0; ii < response.d.length; ii += 2) {
							                I.add(response.d[ii + 1], response.d[ii]);
							            }
							            //debugger;
							            //pages = response.d;
							            if (page && page.length > 0) {
							                I.setValue(page);
							            } else {
							                I.setValue(CurrentPage);
							            }
							        },
							        async: false,
							        error: function (data) {
							            var message = "Error";
							            if (typeof data !== "undefined")
							                message += ": " + data.message;
							            alert(message);
							        },
							    });
							},
						}, {
						    id: 'wikiPages',
							label: E.pages,
							type: 'select',
							style: 'width: 100%;',
							items: [['']],
							size: 5,
							onChange: function () {
							    lp.call(this);
							},
							commit: function (type, element) {
							    if (type == IMAGE && element.result && this.getValue()) element.result.page = this.getValue();
							}
						}]
					}, {
						id: 'selImage',
						type: 'vbox',
						padding: 0,
						children: [{
							id: 'fileProv',
							label: E.providers,
							type: 'select',
							style: 'width: 100%;',
							items: [['']],
							setup: function (type, element) {
								$.ajax({
									context: this,
									type: "POST",
									url: ServicePath + 'GetFilesStorageProviders',
									data: "{}",
									contentType: "application/json; charset=utf-8",
									dataType: "json",
									success: function (response) {
										this.clear();
										for (var ii = 0; ii < response.d.length; ii += 2) {
										    this.add(response.d[ii + 1], response.d[ii]);
										}
									    var prv = qparams["Provider"];
									    if (prv && prv.length > 0) this.setValue(prv);
									    else lp.call(this);
									    //if (response.d.length == 1) this.setAttribute( 'readOnly', true );
									    //else this.setAttribute( 'readOnly', false );
									},
									async: false,
									error: function (data) {
									    //debugger;
										var message = E.error;
										if (typeof data !== "undefined")
											message += ": " + data.message;
										alert(message);
									}
								});
							},
							onChange: function () {
							    lp.call(this);
							}
						}, {
							id: 'wikiImages',
							label: E.images,
							type: 'select',
							style: 'width: 100%;',
							items: [['']],
							size: 5,
							onChange: function () {
								si.call(this);
							},
							validate: CKEDITOR.dialog.validate.notEmpty(E.noSelectedImage),
							commit: function (type, element) {
							    if (type == IMAGE && element.result && this.getValue()) element.result.image = this.getValue();
							    var I = this.getDialog().getContentElement('info', 'linkType');
							    if (type == IMAGE && I && I.getValue() == 'wiki')
							        if (this.getValue() || this.isChanged()) {
							            var D = cl.call(this);;
							            element.data('cke-saved-src', D);
							            element.setAttribute('src', D);
							        }
							},
						}]
					}]
				}]
			}, {
				type: 'vbox',
				id: 'urlOptions',
				children: [{
					type: 'text',
					id: 'url',
					label: E.url,
					'default': '',
					setup: function (type, element) {
					    if (type == IMAGE && element.data('wiki-url-type') && element.data('wiki-url-type') == 'url') {
					        var D = element.data('cke-saved-src');
					        if (!D) D = element.getAttribute('src');
							this.setValue(D);
						}
					},
					commit: function (type, element) {
						var I = this.getDialog().getContentElement('info', 'linkType');
						if (type == IMAGE && I && I.getValue() == 'url') if (this.getValue() || this.isChanged()) {
						    var D = decodeURI(this.getValue());
							element.data('cke-saved-src', D);
							element.setAttribute('src', D);
						}
					},
					onChange: function() {
						si.call(this);
					}
				}]
			}, {
				id: 'basic',
				type: 'hbox',
				height: '250%',
				widths: ['50%', '50%'],
				children: [{
					id: 'selectimage',
					type: 'vbox',
					padding: 0,
					children: [{
						id: 'align',
						type: 'select',
						style: 'width: 100%;',
						label: E.align,
						'default': 'left',
						items: [
							[E.left, 'left'],
							[E.right, 'right'],
						    [E.auto, 'auto']
						],
						setup: function (type, element) {
						    if (type == IMAGE && element.data('wiki-align')) this.setValue(element.data('wiki-align'));
						},
						commit: function (type, element) {
							if (type == IMAGE && (this.getValue() || this.isChanged())) {
							    element.data('wiki-align', this.getValue());
							    if (this.getValue() == 'auto') element.removeStyle('float');
							    else element.setStyle('float', this.getValue());
						    }
						}
					}, {
						id: 'border',
						type: 'select',
						style: 'width: 100%;',
						label: E.bordersType,
						'default': 'frame',
						items: [
							[E.frameless, 'frameless'],
							[E.border, 'border'],
						    [E.frame, 'frame']
						],
						setup: function (type, element) {
						    if (type == IMAGE && element.data('wiki-border')) this.setValue(element.data('wiki-border'));
						},
						commit: function (type, element) {
							if (type == IMAGE && (this.getValue() || this.isChanged())) element.data('wiki-border', this.getValue());
						}
					},{
						type: 'vbox',
						id: 'wikiSize',
						children: [{
							id: 'size',
							type: 'select',
							style: 'width: 100%;',
							label: E.size,
							'default': 'medium',
							items: [
								[E.small, 'small'],
								[E.medium, 'medium'],
								[E.full, 'full'],
								[E.customWidth, 'customWidth'],
								[E.customMax, 'customMax']
							],
							onChange: function (F) {
							   ccw.call(this);
							},
							setup: function (type, element) {
							    if (type == IMAGE && element.data('wiki-type-size')) this.setValue(element.data('wiki-type-size'));
							    ccw.call(this);
							},
							commit: function (type, element) {
							    if (type == IMAGE && (this.getValue() || this.isChanged())) {
									var I = this.getDialog().getContentElement('info', 'linkType');
									if (I && I.getValue() == 'wiki') {
									    element.data('wiki-type-size', this.getValue());
									    element.removeStyle('height');
									    var cs = this.getDialog().getContentElement('info', 'txtCustomSize');
									    switch (this.getValue()) {
									        case 'small':
									            element.setStyle('width', CKEDITOR.tools.cssLength(100));
									            break;
									        case 'medium':
									            element.setStyle('width', CKEDITOR.tools.cssLength(250));
									            break;
									        case 'full':
									            element.removeStyle('width');
									            break;
									        case 'customWidth':
									            if (cs.getValue().length > 0) element.setStyle('width', CKEDITOR.tools.cssLength(cs.getValue()));
									            break;
									        case 'customMax':
									            if (cs.getValue().length > 0) {
									                element.setStyle('width', CKEDITOR.tools.cssLength(cs.getValue()));
									                element.setStyle('height', CKEDITOR.tools.cssLength(cs.getValue()));
									            }
									            break;
									        default:
									    }
									}
								}
							}
						}, {
							type: 'hbox',
							id: 'customSize',
							children: [{
								type: 'text',
								width: '100%',
								id: 'txtCustomSize',
								validate: function() {
									var B = this.getValue().match(i),
									    C = !!(B && parseInt(B[1], 10) !== 0);
									if (!C) alert(E.invalidWidth);
									return C;
								},
								setup: function (type, element) {
								    if (type == IMAGE && element.data('wiki-size-width')
										&& element.data('wiki-url-type') && element.data('wiki-url-type') == 'wiki'
										&& element.data('wiki-type-size')
										&& (element.data('wiki-type-size') == 'customWidth' || element.data('wiki-type-size') == 'customMax')) this.setValue(element.data('wiki-size-width'));
								},
								commit: function (type, element) {
								    //debugger;
									if (type == IMAGE && (this.getValue() || this.isChanged())) {
										var I = this.getDialog().getContentElement('info', 'linkType');
										var J = this.getDialog().getContentElement('info', 'size');
										if (I && I.getValue() == 'wiki' && J && (J.getValue() == 'customWidth' || J.getValue() == 'customMax')) element.data('wiki-size-width', this.getValue());
									} 
								}
							}]
						}]
					}, {
						type: 'vbox',
						id: 'urlSize',
						children: [{
							type: 'hbox',
							widths: ['50%', '50%'],
							children: [{
								type: 'vbox',
								padding: 1,
								children: [{
									type: 'text',
									width: '40px',
									id: 'txtWidth',
									label: E.width,
									onKeyUp: k,
									onChange: function () {
										o.call(this, 'advanced:txtdlgGenStyle');
									},
									validate: function () {
										var B = this.getValue().match(i),
                                            C = !!(B && parseInt(B[1], 10) !== 0);
										if (!C) alert(E.invalidWidth);
										return C;
									},
									setup: r,
									commit: function (type, element, internalCommit) {
										var H = this.getValue();
										if (type == IMAGE) {
										    if (element.result) element.result.width = CKEDITOR.tools.cssLength(H);
										    var I = this.getDialog().getContentElement('info', 'linkType');
										    if (I && I.getValue() == 'url') {
										        if (H) element.setStyle('width', CKEDITOR.tools.cssLength(H));
										        else element.removeStyle('width');
										        !internalCommit && element.removeAttribute('width');
										    }
										} else if (type == PREVIEW) {
											var F = H.match(h);
											if (!F) {
												var G = this.getDialog().originalElement;
												if (G.getCustomData('isReady') == 'true') element.setStyle('width', G.$.width + 'px');
											} else element.setStyle('width', CKEDITOR.tools.cssLength(H));
										} else if (type == g) {
										    element.removeAttribute('width');
											element.removeStyle('width');
										}
									}
								}, {
									type: 'text',
									id: 'txtHeight',
									width: '40px',
									label: E.height,
									onKeyUp: k,
									onChange: function () {
										o.call(this, 'advanced:txtdlgGenStyle');
									},
									validate: function () {
										var B = this.getValue().match(i),
                                            C = !!(B && parseInt(B[1], 10) !== 0);
										if (!C) alert(E.invalidHeight);
										return C;
									},
									setup: r,
									commit: function (type, element, internalCommit) {
										var H = this.getValue();
										if (type == IMAGE) {
										    if (element.result) element.result.height = CKEDITOR.tools.cssLength(H);
										    var I = this.getDialog().getContentElement('info', 'linkType');
										    if (I && I.getValue() == 'url') {
										        if (H) element.setStyle('height', CKEDITOR.tools.cssLength(H));
										        else element.removeStyle('height');
										        !internalCommit && element.removeAttribute('height');
										    }
										} else if (type == PREVIEW) {
											var F = H.match(h);
											if (!F) {
												var G = this.getDialog().originalElement;
												if (G.getCustomData('isReady') == 'true') element.setStyle('height', G.$.height + 'px');
											} else element.setStyle('height', CKEDITOR.tools.cssLength(H));
										} else if (type == g) {
										    element.removeAttribute('height');
										    element.removeStyle('height');
										}
									}
								}]
							}, {
								id: 'ratioLock',
								type: 'html',
								style: 'margin-top:30px;width:40px;height:40px;',
								onLoad: function () {
									var B = CKEDITOR.document.getById(x),
                                        C = CKEDITOR.document.getById(w);
									if (B) {
										B.on('click', function (D) {
											q(this);
											D.data && D.data.preventDefault();
										}, this.getDialog());
										B.on('mouseover', function () {
											this.addClass('cke_btn_over');
										}, B);
										B.on('mouseout', function () {
											this.removeClass('cke_btn_over');
										}, B);
									}
									if (C) {
										C.on('click', function (D) {
											var I = this;
											var J = p(I),
												F = I.originalElement,
                                                G = I.getValueOf('info', 'txtWidth');
											if (F.getCustomData('isReady') == 'true' && G) {
												var H = F.$.height / F.$.width * G;
												if (!isNaN(H)) {
													I.setValueOf('info', 'txtHeight', Math.round(H));
													l(I);
												}
											}
											D.data && D.data.preventDefault();
										}, this.getDialog());
										C.on('mouseover', function () {
											this.addClass('cke_btn_over');
										}, C);
										C.on('mouseout', function () {
											this.removeClass('cke_btn_over');
										}, C);
									}
								},
								html: '<div><a href="javascript:void(0)" tabindex="-1" title="' + E.lockRatio + '" class="cke_btn_locked" id="' + w + '" role="checkbox"><span class="cke_icon"></span><span class="cke_label">' + E.lockRatio + '</span></a>' + '<a href="javascript:void(0)" tabindex="-1" title="' + E.resetSize + '" class="cke_btn_reset" id="' + x + '" role="button"><span class="cke_label">' + E.resetSize + '</span></a>' + '</div>'
							}]
						}]
					}]
				}, {
					id: 'selectedimage',
					type: 'vbox',
					padding: 0,
					children: [{
						type: 'html',
						id: 'htmlPreview',
						style: 'width:95%;',
						html: '<div>' + CKEDITOR.tools.htmlEncode(E.preview) + '<br>' + '<div id="' + y + '" class="ImagePreviewLoader" style="display:none"><div class="loading">&nbsp;</div></div>' + '<div class="ImagePreviewBox"><table><tr><td>' + '<a href="javascript:void(0)" target="_blank" onclick="return false;" id="' + z + '">' + '<img id="' + A + '" alt="" /></a>' + ('') + '</td></tr></table></div></div>'
					}]
				}]

			}, {
				id: 'txtDescr',
				type: 'text',
				label: E.description,
				style: 'width: 100%',
				'default': '',
				setup: function (type, element) {
				    if (type == IMAGE && element.data('wiki-desc')) this.setValue(element.data('wiki-desc'));
				},
				commit: function (type, element) {
				    if (type == IMAGE && (this.getValue() || this.isChanged())) element.data('wiki-desc', this.getValue());
				}
			}]
		},{
		        id: 'option',
		        label: E.option,
		        title: E.option,
		        elements: [{
		            id: 'alt',
		            type: 'text',
		            label: E.alt,
		            style: 'width: 100%',
		            'default': '',
		            setup: function (type, element) {
		                if (type == IMAGE && element.getAttribute('alt')) this.setValue(element.getAttribute('alt'));
		            },
		            commit: function (type, element) {
		                if (type == IMAGE && (this.getValue() || this.isChanged())) element.setAttribute('alt', this.getValue());
		                if (type == IMAGE && element.result && this.getValue()) element.result.alt = this.getValue();
		            }
		        }, {
		            id: 'link',
		            type: 'text',
		            label: E.link,
		            style: 'width: 100%',
		            'default': '',
		            setup: function (type, element) {
		                if (type == IMAGE && element.data('wiki-link')) this.setValue(element.data('wiki-link'));
		            },
		            commit: function (type, element) {
		                if (type == IMAGE && (this.getValue() || this.isChanged())) element.data('wiki-link', this.getValue());
		            }
		        }]
		    }]

	};

});

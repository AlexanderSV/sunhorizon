﻿CKEDITOR.plugins.add('wiki_image',
{
	requires: ['iframedialog'],
	lang: ['en', 'ru'],
	init: function (editor) {

		//		var o = function(q, r, s) {
		//			editor.addCommand(r, new CKEDITOR.dialogCommand(r));
		//			editor.ui.addButton(q, {
		//				label: 'Insert image', //n.common[q.charAt(0).toLowerCase() + q.slice(1)],
		//				command: r,
		//				icon: this.path + 'images/anchor.gif'
		//			});
		//			CKEDITOR.dialog.add(r, s);
		//		};

		//		o('Wiki_image', 'wiki_image', this.path + 'dialogs/wiki_image.js');
		//		o('Wiki_imagebutton', 'wiki_imagebutton', this.path + 'dialogs/wiki_image.js');

		var pluginName = 'wiki_image';
		editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
		CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/wiki_image.js');
		editor.ui.addButton('Wiki_image',
            {
            	label: editor.lang.wiki_image.buttonName,
            	command: pluginName,
            	icon: this.path + 'images/image.gif'
            });

		if (editor.addMenuItems) {
			editor.addMenuItems(
				{
					wikiimage:
					{
						label: editor.lang.wiki_image.menuItemName,
						command: pluginName,
						icon: this.path + 'images/image.gif',
						group: 'wikiimage' // add to config
					}
				});
		}
		if (editor.contextMenu) {
			editor.contextMenu.addListener(function (q) {
				if (q && !q.isReadOnly()) {
					var r = q.getName();
					if (r == 'img' && q.getAttribute('wikiid') == 'image') 
						return {
							wikiimage: 2
						};
				}
				//				if (!element || !element.is('img'))
				//					return null;
				//				return { wikiimage: CKEDITOR.TRISTATE_OFF };
			});
		}

	}
});

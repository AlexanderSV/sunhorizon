﻿CKEDITOR.plugins.setLang('wiki_specialtags', 'ru', {
    wiki_specialtags: {
        label: 'Специальные теги',
        wikiTitle: 'Название вики',
        up: '',
        top: 'Линк на верх страницы',
        toc: 'Меню страницы',
        themePath: 'Путь к текущей теме',
        rssPage: 'URL на страницу RSS',
        wikiVersion: 'Версия SunHorizon Wiki',
        mainUrl: 'URL на вики',
        pageCountNamespace: 'Кол-во страницы в текущем разделе',
        pageCountWiki: 'Кол-во страниц в вики',
        userName: 'Текущий пользователь',
        loginLogout: 'Линк на регистрацию/выход',
        clear: '',
        cloud: 'Page categories cloud',
        searchBox: '',
        pageName: 'Название текущей страницы',
        namespace: 'Текущий раздел',
        categories: 'Категории',
        namespaceDropDown: 'Выпадающий список разделов',
        namespaceList: 'Список разделов',
        orphans: 'Страницы не имеющие ссылок на них',
        wanted: 'Отсутствующие страницы',
        incoming: 'Страницы, ссылающиеся на текущую страницу',
        outgoing: 'Страницы, на которые ссылается текущая страница',
        recentChanges: 'Список последних изменений в разделе',
        recentChangesEntire: 'Список последних изменений в вики'
    }
});
﻿CKEDITOR.plugins.add('wiki_specialtags',
	{
	    requires: ['richcombo'], //, 'styles' ],
	    lang: ['en', 'ru'],
		init: function(editor) {
			var config = editor.config,
			    lang = editor.lang.format;

			var lng = editor.lang.wiki_specialtags;
			// Gets the list of tags from the settings.
			var tags = []; //new Array();
			//this.add('value', 'drop_text', 'drop_label');
			tags[0] = ["{WIKITITLE}", lng.wikiTitle, lng.wikiTitle];
			tags[1] = ["{UP}", lng.up, lng.up];
			tags[2] = ["{TOP}", lng.top, lng.top];
			tags[3] = ["{TOC}", lng.toc, lng.toc];
			tags[4] = ["{THEMEPATH}", lng.themePath, lng.themePath];
			tags[5] = ["{RSSPAGE}", lng.rssPage, lng.rssPage];
			tags[6] = ["{WIKIVERSION}", lng.wikiVersion, lng.wikiVersion];
			tags[7] = ["{MAINURL}", lng.mainUrl, lng.mainUrl];
			tags[8] = ["{PAGECOUNT}", lng.pageCountNamespace, lng.pageCountNamespace];
			tags[9] = ["{PAGECOUNT(*)}", lng.pageCountWiki, lng.pageCountWiki];
			tags[10] = ["{USERNAME}", lng.userName, lng.userName];
			tags[11] = ["{LOGINLOGOUT}", lng.loginLogout, lng.loginLogout];
			tags[12] = ["{CLEAR}", lng.clear, lng.clear];
			tags[13] = ["{CLOUD}", lng.cloud, lng.cloud];
			tags[14] = ["{SEARCHBOX}", lng.searchBox, lng.searchBox];
			tags[15] = ["{PAGENAME}", lng.pageName, lng.pageName];
			tags[16] = ["{NAMESPACE}", lng.namespace, lng.namespace];
			tags[17] = ["{CATEGORIES}", lng.categories, lng.categories];
			tags[18] = ["{NAMESPACEDROPDOWN}", lng.namespaceDropDown, lng.namespaceDropDown];
			tags[19] = ["{NAMESPACELIST}", lng.namespaceList, lng.namespaceList];
			tags[20] = ["{ORPHANS}", lng.orphans, lng.orphans];
			tags[21] = ["{WANTED}", lng.wanted, lng.wanted];
			tags[22] = ["{INCOMING}", lng.incoming, lng.incoming];
			tags[23] = ["{OUTGOING}", lng.outgoing, lng.outgoing];
			tags[24] = ["{RECENTCHANGES}", lng.recentChanges, lng.recentChanges];
			tags[25] = ["{RECENTCHANGES(*)}", lng.recentChangesEntire, lng.recentChangesEntire];
		    

			// Create style objects for all defined styles.

			editor.ui.addRichCombo('Wiki_specialTags',
				{
				    label: lng.label, //"Insert special tags",
				    title: lng.label,
				    voiceLabel: lng.label,
					className: 'cke_format',
					multiSelect: false,
				    
					panel:
					{
						css: [config.contentsCss, CKEDITOR.getUrl(editor.skinPath + 'editor.css')],
						voiceLabel: lang.panelVoiceLabel
					},

					init: function() {
					    this.startGroup(lng.label);
						//this.add('value', 'drop_text', 'drop_label');
						for (var this_tag in tags) {
							this.add(tags[this_tag][0], tags[this_tag][1], tags[this_tag][2]);
						}
					},

					onClick: function(value) {
						editor.focus();
						editor.fire('saveSnapshot');
						editor.insertHtml(value);
						editor.fire('saveSnapshot');
					}
				});
		}
	});
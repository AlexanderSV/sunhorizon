﻿CKEDITOR.plugins.add('wiki_h4',
{
	init: function (editor) {
	    editor.addCommand('wiki_h4', {
	        exec: function(editor) {
	            var selectedText = editor.getSelection().getNative();
	            editor.insertText("=====" + selectedText + "=====");
	        }
	    });
	    editor.ui.addButton('Wiki_h4',
            {
                label: editor.lang.format.tag_h4,
                command: 'wiki_h4',
            	icon: this.path + 'h4.png'
            });
           }
});


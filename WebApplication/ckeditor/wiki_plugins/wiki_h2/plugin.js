﻿CKEDITOR.plugins.add('wiki_h2',
{
	init: function (editor) {
	    editor.addCommand('wiki_h2', {
	        exec: function(editor) {
	            var selectedText = editor.getSelection().getNative();
	            editor.insertText("===" + selectedText + "===");
	        }
	    });
	    editor.ui.addButton('Wiki_h2',
            {
                label: editor.lang.format.tag_h2,
                command: 'wiki_h2',
            	icon: this.path + 'h2.png'
            });
           }
});


﻿CKEDITOR.plugins.add('wiki_hrtitle',
{
    // TODO: localize
    //lang: ['en', 'ru'],
    init: function (editor) {  
        editor.addCommand('wiki_hrtitle', {
            exec: function (editor) {
            editor.insertText("----");
            }
        });
        editor.ui.addButton('Wiki_hrtitle',
            {
                label: '', //editor.lang.hrtitle.buttonName,
                command: 'wiki_hrtitle',
                icon: this.path + 'hr.png'
            });
    }
});

﻿CKEDITOR.plugins.setLang('rating', 'en', {
    rating: {
        buttonName: 'Insert/edit rating',
        menuItemName: 'Edit rating',
        title: 'Insert/edit rating',
        forCurrentPage: 'For current page',
        fromOtherPage: 'From other page',
        namespaces: 'Namespaces',
        pages: 'Pages',
    }
});
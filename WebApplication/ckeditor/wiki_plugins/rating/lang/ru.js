﻿CKEDITOR.plugins.setLang('rating', 'ru', {
    rating: {
        buttonName: 'Вставка/редактирование рейтинга',
        menuItemName: 'Редактирование рейтинга',
        title: 'Вставка/редактирование рейтинга',
        forCurrentPage: 'Для текущей страницы',
        fromOtherPage: 'С другой страницы',
        namespaces: 'Разделы',
        pages: 'Страницы',
    }
});
﻿CKEDITOR.plugins.add('wiki_underlined',
{
	init: function (editor) {
	    editor.addCommand('wiki_underlined', {
	        exec: function(editor) {
	            var selectedText = editor.getSelection().getNative();
	            editor.insertText("__" + selectedText + "__");
	        }
	    });
		editor.ui.addButton('Wiki_underlined',
            {
                label: editor.lang.underline,
                command: 'wiki_underlined',
            	icon: this.path + 'underlined.png'
            });
           }
});


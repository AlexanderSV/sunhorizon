/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
var ckeditorBasePath = CKEDITOR.basePath.substr(0, CKEDITOR.basePath.indexOf("ckeditor/"));
var customPluginsRoot = ckeditorBasePath + 'ckeditor/wiki_plugins/';

CKEDITOR.plugins.addExternal('wiki_link', customPluginsRoot + 'wiki_link/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_image', customPluginsRoot + 'wiki_image/', 'plugin.js');

CKEDITOR.plugins.addExternal('wiki_bold', customPluginsRoot + 'wiki_bold/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_italic', customPluginsRoot + 'wiki_italic/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_underlined', customPluginsRoot + 'wiki_underlined/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_striked', customPluginsRoot + 'wiki_striked/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_h1', customPluginsRoot + 'wiki_h1/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_h2', customPluginsRoot + 'wiki_h2/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_h3', customPluginsRoot + 'wiki_h3/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_h4', customPluginsRoot + 'wiki_h4/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_superscript', customPluginsRoot + 'wiki_superscript/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_subscript', customPluginsRoot + 'wiki_subscript/', 'plugin.js');
CKEDITOR.plugins.addExternal('wiki_specialtags', customPluginsRoot + 'wiki_specialtags/', 'plugin.js');


//CKEDITOR.plugins.addExternal('rating', customPluginsRoot + 'rating/', 'plugin.js');

CKEDITOR.editorConfig = function (config) {

    config.customConfig = ckeditorBasePath + 'ckeditor/custom_wiki_plugins/wiki_config.js';


    //config.removeFormatTags = 'b,big,code,del,dfn,em,font,i,ins,kbd,ol,ul,li';
    //config.protectedSource.push(/(<*>)/gi);

    //config.entities = false;
    //config.htmlEncodeOutput = false;

    CKEDITOR.config.forcePasteAsPlainText = true; // Paste only text

    // disable tag p
    config.enterMode = CKEDITOR.ENTER_BR;
    config.shiftEnterMode = CKEDITOR.ENTER_BR;

    config.format_tags = 'h1;h2;h3;h4';
    config.stylesSet = ['default',
        { name: 'code', element: 'code' },
        { name: 'pre', element: 'pre' },
        { name: 'separator', element: 'h1', attributes: { 'class': 'separator' } }
    ];

    //config.FormatSource = false;
    //config.FormatOutput = false;
    //config.protectedSource.push(/<br[\s\S]*?\/>/g);   // BR Tags
    //config.protectedSource.push(/<img[\s\S]*?\/>/g);   // IMG Tags

    // Define changes to default configuration here. For example:
    //config.language = 'ru';
    // config.uiColor = '#AADC6E';
    config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'wiki_link';
    config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'wiki_image';

    config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'wiki_bold,wiki_italic,wiki_underlined,wiki_striked,wiki_h1,wiki_h2,wiki_h3,wiki_h4,wiki_superscript,wiki_subscript,wiki_specialtags';

    config.menu_groups = config.menu_groups + ',wikilink,wikiimage,otherPlugins';

    //config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'rating';

    config.removePlugins = [
//    'a11yhelp',
//'about',
//    'bidi',
//    'contextmenu',
//    'dialogadvtab',
//    'div',
//   'elementspath',
//   'filebrowser',
//    'find',
'flash',
'forms',
//   'horizontalrule',
'image',
'link',
'maximize',
'newpage',
//    'pagebreak',
//    'pastefromword',
//    'pastetext',
//    'popup',
'save',
//    'scayt',
//    'showborders',
//    'specialchar',
//    'table',
'templates'
//    'wsc',
//    'tab'
    ].join(',');

};

CKEDITOR.on('instanceReady', function (ev) {

    //var editor = ev.editor;
    //var dataProcessor = editor.dataProcessor;
    //var htmlFilter = dataProcessor && dataProcessor.htmlFilter;
    //htmlFilter.addRules(
    //{
    //    elements:
    //    {
    //        input: function (element) {
    //            return false;
    //        },
    //    }
    //});

    //ev.editor.dataProcessor = new CKEDITOR.htmlDataProcessor();
    //ev.editor.dataProcessor.toDataFormat = function(html, fixForBody) {
    //    return html;
    //};
    //ev.editor.dataProcessor.toHtml = function (data, fixForBody) {
    //    return data;
    //};
    //ev.editor.dataProcessor.removeAll();
    var tags = ['p', 'ol', 'ul', 'li', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'a', 'img', 'div', 'table', 'tbody', 'tr', 'td', 'thead', 'th', 'caption', 'pre', 'code'];

    for (var key in tags) {
        ev.editor.dataProcessor.writer.setRules(tags[key],
            {
                indent: false,
                breakBeforeOpen: false,
                breakAfterOpen: false,
                breakBeforeClose: false,
                breakAfterClose: false
            });
    }

    //ev.editor.dataProcessor.writer.setRules('p',
    //    {
    //        indent: false,
    //        breakBeforeOpen: true,
    //        breakAfterOpen: false,
    //        breakBeforeClose: false,
    //        breakAfterClose: true
    //    });

});



//CKEDITOR.htmlDataProcessor = function (editor) {
//    this.editor = editor;
//    this.writer = new CKEDITOR.htmlWriter();
//    //this.dataFilter = new CKEDITOR.htmlParser.filter();
//    //this.htmlFilter = new CKEDITOR.htmlParser.filter();
//    this.dataFilter.removeAll();
//    this.htmlFilter.removeAll();
//    this.htmlFilter.addRules(
//        {
//            elements:
//            {
//                input: function(element) {
//                    return false;
//                },
//            }
//        });
//    this.dataFilter.addRules(
//    {
//        elements:
//        {
//            input: function (element) {
//                return false;
//            },
//        }
//    });

//    //this.dataFilter.addRules({ elements: {} });
//    //this.htmlFilter.addRules({ elements: {} });
//};

//CKEDITOR.htmlDataProcessor.prototype =
//{
//    toHtml: function (data, fixForBody) {
//        // all converting to html (like: data = data.replace( /</g, '&lt;' );)
//        return data;
//    },

//    toDataFormat: function (html, fixForBody) {
//        // all converting from html (like: html = html.replace( /<br><\/p>/gi, '\r\n');)
//        return html;
//    }
//};

// http://ckeditor.com/forums/CKEditor-3.x/different-HTML-processor

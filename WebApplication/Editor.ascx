<%@ Control Language="C#" AutoEventWireup="true" Inherits="ScrewTurn.Wiki.Editor" Codebehind="Editor.ascx.cs" %>

<%@ Register TagName="ClientImageBrowser" TagPrefix="st" Src="~/ClientImageBrowser.ascx" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Literal ID="lblStrings" runat="server" meta:resourcekey="lblStringsResource3" />

<!-- added for WYSIWYG -->
<script type="text/javascript">
<!--
	var debug=true;

	var iframe;
	var inWYSIWYG=false;

	//function _IsInWYSIWYG() {
	//  if(inWYSIWYG) {
	//	inWYSIWYG=false;
	//	return true;
	//  }
	//  return false;
	//}

	function iframe_onload() {
	    inWYSIWYG = true;
	    alert(document.getElementById(VisualControl).value);
	    CKEditorWYSIWYG.Text = document.getElementById(VisualControl).value;
	}

	function IncreaseHeight(elemName) {
		var elem = document.getElementById(elemName);
		var pos = AbsolutePosition(elem);
		elem.style["height"] = pos.height + 100 + "px";

		__CreateCookie("ScrewTurnWikiES", elem.style["height"], 365);
		
		return false;
	}
	function DecreaseHeight(elemName) {
		var elem = document.getElementById(elemName);
		var pos = AbsolutePosition(elem);
		if(pos.height > 100) elem.style["height"] = pos.height - 100 + "px";

		__CreateCookie("ScrewTurnWikiES", elem.style["height"], 365);
		
		return false;
	}

	function InitES() {
		var cookieValue = __ReadCookie("ScrewTurnWikiES");
		if(cookieValue) {
			var elem = document.getElementById(MarkupControl);
			if(elem) elem.style["height"] = cookieValue;
			elem = document.getElementById("iframe");
			if(elem) elem.style["height"] = cookieValue;
		}
	}

	function __FocusEditorWindow() {
		$("#<%= CKEditorMarkup.ClientID %>").focus();
		$("#iframe").focus();
	}
// -->
</script>

<div id="TabContainerDiv">
	<asp:Button ID="btnWikiMarkup" runat="server" Text="WikiMarkup" OnClick="btnWikiMarkup_Click" CssClass="tabbutton" CausesValidation="false"
		EnableViewState="False" meta:resourcekey="btnWikiMarkupResource3" />
	<asp:Button ID="btnVisual" runat="server" Text="Visual" OnClick="btnVisual_Click" CssClass="tabbutton" CausesValidation="false"
		EnableViewState="False" meta:resourcekey="btnVisualResource3" />
	<asp:Button ID="btnPreview" runat="server" Text="Preview" OnClick="btnPreview_Click" CssClass="tabbutton" CausesValidation="false"
		EnableViewState="False" meta:resourcekey="btnPreviewResource3" />
	<span id="ProgressSpan" style="display: none;">
		<img src="Images/Editor/Progress.gif" alt="Please wait..." style="margin-bottom: -2px;" />
	</span>
</div>

<asp:MultiView ID="mlvEditor" runat="server">

	<asp:View ID="viwStandard" runat="server">
	
		
		<%--<asp:TextBox ID="txtMarkup" runat="server" TextMode="MultiLine" Width="99%" Height="400px" style="width: 700px; min-width: 99%; max-width: 99%;" meta:resourcekey="txtMarkupResource3" />--%>
	
        <div id="MarkupDiv">
		    <CKEditor:CKEditorControl ID="CKEditorMarkup" runat="server" style="width: 100%; height: 400px;" BasePath="~/ckeditor" Height="400px"></CKEditor:CKEditorControl>
		</div>
	</asp:View>
	
	<asp:View ID="viwVisual" runat="server">
		
		<div id="WysiwygDiv">
		    <CKEditor:CKEditorControl ID="CKEditorWYSIWYG" runat="server" style="width: 100%; height: 400px;" BasePath="~/ckeditor" Height="400px"></CKEditor:CKEditorControl>
<%--			<iframe id="iframe" name="iframe" onload="javascript:return iframe_onload();" src="IframeEditor.aspx" style="width: 100%; height: 400px;" frameborder="0"></iframe>--%>
		</div>
		
<%--		<div style="display: none;">
			<asp:TextBox ID="lblWYSIWYG" runat="server" TextMode="MultiLine" meta:resourcekey="lblWYSIWYGResource1" />
		</div>--%>
	</asp:View>
	
	<asp:View ID="viwPreview" runat="server">
		
		<div class="toolbar" style="padding-top: 8px; padding-bottom: 0px;">
			<asp:Label ID="lblPreviewWarning" runat="server" CssClass="resulterror" EnableViewState="False"
				Text="&lt;b&gt;Warning&lt;/b&gt;: this is only a preview. The content was not saved." meta:resourcekey="lblPreviewWarningResource3" />
		</div>
	
		<div id="PreviewDiv" style="border: solid 4px #999999; padding: 8px; height: 450px; overflow: auto;">
			<asp:Literal ID="lblPreview" runat="server" EnableViewState="False" meta:resourcekey="lblPreviewResource3" />
		</div>
	</asp:View>

</asp:MultiView>

<%--<div id="SnippetsMenuDiv" class="menucontainer" style="display: none;">
	<asp:Label ID="lblSnippets" runat="server" EnableViewState="False" meta:resourcekey="lblSnippetsResource3" />
</div>--%>


<script type="text/javascript">
<!--

	function AbsolutePosition(obj) {
		var pos = null; 
		if(obj != null) {
			pos = new Object();
			pos.top = obj.offsetTop;
			pos.left = obj.offsetLeft;
			pos.width = obj.offsetWidth;
			pos.height= obj.offsetHeight;
			obj = obj.offsetParent;
			while(obj != null) {
				pos.top += obj.offsetTop;
				pos.left += obj.offsetLeft;
				obj = obj.offsetParent;
			}
		}
		return(pos);
	}

	InitES();
// -->
</script>

<asp:Literal ID="lblToolbarInit" runat="server" />

<div id="SnippetsMenuDiv0" class="menucontainer" style="display: none;">
	<asp:Label ID="lblSnippets0" runat="server" EnableViewState="False" meta:resourcekey="lblSnippetsResource3" />
</div>

<script type="text/javascript">
<!--

	function AbsolutePosition(obj) {
		var pos = null; 
		if(obj != null) {
			pos = new Object();
			pos.top = obj.offsetTop;
			pos.left = obj.offsetLeft;
			pos.width = obj.offsetWidth;
			pos.height= obj.offsetHeight;
			obj = obj.offsetParent;
			while(obj != null) {
				pos.top += obj.offsetTop;
				pos.left += obj.offsetLeft;
				obj = obj.offsetParent;
			}
		}
		return(pos);
	}

	InitES();
// -->
</script>

<asp:Literal ID="lblGlobalJSParametrs" runat="server" />

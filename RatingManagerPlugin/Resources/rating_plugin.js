﻿CKEDITOR.plugins.add('rating',
{

    requires: ['iframedialog'],
    lang: ['en', 'ru'],
    init: function (editor) {
        var pluginName = 'rating';
        editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
        CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/rating.js');
        editor.ui.addButton('rating',
            {
                label: editor.lang.rating.buttonName,
                command: pluginName,
                icon: this.path + 'images/image.gif'
            });

        if (editor.addMenuItems) {
            editor.addMenuItems(
				{
				    ratingM:
					{
					    label: editor.lang.rating.menuItemName,
					    command: pluginName,
					    icon: this.path + 'images/image.gif',
					    group: 'otherPlugins'
					}
				});
        }
        if (editor.contextMenu) {
            editor.contextMenu.addListener(function (q) {
                if (q && !q.isReadOnly()) {
                    var r = q.getName();
                    if (r == 'img' && q.getAttribute('pluginid') == 'rating')
                        return {
                            ratingM: 2
                        };
                }
            });
        }

    }
});

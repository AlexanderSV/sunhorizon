﻿CKEDITOR.dialog.add('rating', function (editor) {
    var curNs = CurrentNamespace && CurrentNamespace != "" ? CurrentNamespace : "root";
    var page;
    var source;
    var pluginid = 'rating';

    var E = editor.lang.rating;
    return {
        title: E.title,
        minWidth: 300,
        minHeight: 200,
        onShow: function () {
            //debugger;
            var t = this;
            t.editMode = false;
            var parent = t.getParentEditor(),
			    selection = parent.getSelection(),
                element = selection && selection.getStartElement();
            if (element && element.getName() == 'img' && element.getAttribute('pluginid') && element.getAttribute('pluginid') == pluginid) {
                t.editMode = true;
                t.element = element;
                page = element.getAttribute('page');
                if (page && page.length > 0) source = 'other'; else source = 'current';
                var pos = page.indexOf('.');
                if (-1 < pos) {
                    curNs = page.substring(0, pos); 
                } else {
                    curNs = CurrentNamespace && CurrentNamespace != "" ? CurrentNamespace : "root";
                }
            }
            this.setupContent();
        },
        onOk: function () {
            //debugger;
            var t = this;           
            var result = {};
            t.commitContent(result);
            
            var pageName = '';
            if (result.source == 'other') pageName = result.page;
            //var style = 'background-image: url(rating_images_image.gif); background-repeat: repeat-x; width: 80px; height: 16px; border: solid 1px black';

            if (IsInWYSIWYG) {
                 if (!t.editMode) {
                    t.element = editor.document.createElement('img');
                    t.element.setAttribute('pluginid', pluginid);
                    t.element.setAttribute('src', 'url(stars.gif.ashx)');
                     //t.element.setAttribute('style', style);
                 }
                t.element.setAttribute('page', pageName);

                if (!t.editMode) editor.insertElement(t.element);               
            } else {
                //editor.insertText("<div page=\"" + pageName + "\" pluginid=\"" + pluginid + "\" style=\"" + style + "\"></div>");
                if (pageName == '') {
                    editor.insertText("{rating}");
                } else {
                    editor.insertText("{rating|" + decodeURI(pageName) + "}");
                }
            }
        },
        contents: [{
            id: 'info',
            //label: E.info,
            //title: E.info,
            elements: [{
                id: 'source',
                type: 'select',
                label: E.type,
                'default': 'current',
                items: [
                    [E.forCurrentPage, 'current'],
                    [E.fromOtherPage, 'other']
                ],
                onChange: function () {
                    var val = this.getValue();
                    var L = this.getDialog().getContentElement('info', 'ratingOptions');
                    if (L) {
                        L = L.getElement().getParent().getParent();
                        if (val == 'other') L.show();
                        else L.hide();
                    }
                },
                setup: function () {
                    this.setValue(source);
                },
                commit: function (result) {
                    result.source = this.getValue();
                }
            }, {
                type: 'vbox',
                id: 'ratingOptions',
                children: [{                       
                    id: 'wikiNsList',
                    label: E.namespaces,
                    type: 'select',
                    style: 'width: 100%;',
                    items: [['']],
                    setup: function () {
                        $.ajax({
                            context: this,
                            type: "POST",
                            url: ServicePath + 'GetNamespaces',
                            data: "{}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var t = this;
                                t.clear();
                                for (var ii = 0; ii < response.d.length; ii += 2) {
                                    t.add(response.d[ii + 1], response.d[ii]);
                                }
                                //debugger;
                                t.setValue(curNs);
                            },
                            async: false,
                            error: function (data) {
                                var message = "Error";
                                if (typeof data !== "undefined")
                                    message += ": " + data.message;
                                alert(message);
                            },
                        });
                    },
                    onChange: function () {
                        $.ajax({
                            context: this,
                            type: "POST",
                            url: ServicePath + 'GetPages',
                            data: "{ selectedNs: '" + this.getValue() + "' }",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var I = this.getDialog().getContentElement('info', 'wikiPages');
                                I.clear();
                                for (var ii = 0; ii < response.d.length; ii += 2) {
                                    I.add(response.d[ii + 1], response.d[ii]);
                                }
                                //pages = response.d;
                                //debugger;
                                if (page && page.length > 0) {
                                    I.setValue(page);
                                }
                            },
                            async: false,
                            error: function (data) {
                                var message = "Error";
                                if (typeof data !== "undefined")
                                    message += ": " + data.message;
                                alert(message);
                            },
                        });
                    },
                    commit: function (result) {
                        result.namespace = this.getValue();
                    }
                }, {
                    id: 'wikiPages',
                    label: E.pages,
                    type: 'select',
                    style: 'width: 100%;',
                    items: [['']],
                    size: 7,
                    commit: function (result) {
                        result.page = this.getValue();
                    }
                }]
            }]
        }]

    };
});

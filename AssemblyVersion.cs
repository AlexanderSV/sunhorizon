
using System.Reflection;

[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("SunHorizon Wiki")]
[assembly: AssemblyCopyright("Copyright � Threeplicate Srl 2006-2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.2.1.17")]
[assembly: AssemblyFileVersion("1.2.1.17")]
